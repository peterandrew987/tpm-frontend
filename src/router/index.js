import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/pages/login/login'
import HelloWorld from '@/pages/HelloWorld'
import Try from '@/pages/try'
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/helo',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/try',
      name: 'try',
      component: Try
    }
  ],
  linkExactActiveClass: 'active'
})
